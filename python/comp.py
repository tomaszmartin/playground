# Only the last item is different
a = (1, 2, 3)
b = (1, 2, 4)

assert a < b

# The first item is already different
# but the last one indicates different
# order that the first one
a = (1, 2, 3)
b = (3, 2, 1)

assert a < b
