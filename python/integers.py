import ctypes
import sys
import math

# CPython looks up values for small integers in a small_ints array.
# This way values from small ints (-5, 256) are always the same
# i.e. have the same address
i = 256
assert i is 256

# Throws an error when run from terminal
# but works fine when run as script
i = 257
assert i is 257

# Moving values around in memory
def replace(old, new):
    src = id(new)
    dest = id(old)
    count = sys.getsizeof(new)
    ctypes.memmove(dest, src, count)


old, new = 777, 999
print(f"Replace {old} with {new}")
replace(old, new)
print(f"Now it's {old}")
